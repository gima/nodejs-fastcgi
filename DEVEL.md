## remember

echo > .nodejs-fcgi.log

chmod 755 nodejs.fcgi
chmod 644 .htaccess
chmod 644 .htaccess-wwwdir
chmod 622 .nodejs-fcgi.log
chmod u=rwX,go=rX node_modules
