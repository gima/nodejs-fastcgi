#!/path/to/node
// ^ Change this path to a the nodejs binary.
//   The binary must be accessible by the server that is executing this FastCGI program.

/*
The purpose of this FastCGI program is to execute the .js file given to it in PATH_INFO[1] (relative to DOCUMENT_ROOT[2]).

USAGE & HOMEPAGE: https://github.com/gima/nodejs-fastcgi

[1] https://en.wikipedia.org/wiki/Common_Gateway_Interface
[2] https://httpd.apache.org/docs/current/mod/mod_cgi.html
*/

// ----------------------------------------------------------------------------
// Implementation and technical details follow:

/*
Apache (or some other FastCGI executor) starts N copies of this program to handle HTTP requests.
Requests are received through STDIN and answered through STDOUT, in FCGI protocol.
Apache's configuration decides how long this program stays alive until it's terminated.
(For this reason, if this program is being developed, set TIMEOUT to a value after-which this program is forcibly self-terminated.
 This is to force Apache to start a new instance of this program for every request, so changes to this file are seen immediately,
 instead of answers from the already-running copy of this program.
 TIMEOUT of 500ms ought to be enough for the current request to have been completed.)
*/

'use strict'

const TIMEOUT = undefined
const fs = require('fs')

// Write uncaught exceptions to a file in the current working directory (which is probably where this file resides).
process.on('uncaughtException', function (err) {
  fs.appendFileSync('.nodejs-fcgi.log', `${new Date().toISOString()} ${err.stack}\n`)
  process.exit(1)
})


// Load the `node-fastcgi` library available from: https://github.com/fbbdev/node-fastcgi
const fcgi = require('node-fastcgi')

// Listen STDIN for incoming FCGI-requests (HTTP requests)
fcgi.createServer(function(req, res) {
  // Path to the nodejs script-to-be-executed is received via PATH_INFO
  // (Request looks like "http://example.com/handlers/nodejs.fcgi/path/to/script.js",
  //  which would lead to executing script in "DOCUMENT_ROOT/path/to/script.js".
  let path = getScriptPath(req)

  // Test whether the script was found (or allowed).
  let pass
  try {
    pass = path && fs.statSync(path).isFile()
  } catch (e) {
    pass = false
  }
  if (!pass) {
    res.writeHead(404, { 'Content-Type': 'text/plain' })
    res.end('Not Found')
    // If timeout is set, terminate this process after said timeout.
    TIMEOUT && setTimeout(_=> process.exit(0), TIMEOUT)
    return
  }

  // Execute the script using NodeJS's require().
  // Each script should export HTTP request handler in the form of "module.exports = function(request, response) { .."
  require(path)(req,res)

  // After request, delete all cached files in NodeJS's "require() cache", so that further executions
  // of said files read fresh copies, rather than stale cached ones.
  for (let k of Object.keys(require.cache)) delete(require.cache[k])
  // Same as above.
  TIMEOUT && setTimeout(_=> process.exit(0), TIMEOUT)
}).listen()

function getScriptPath(req) {
  const docroot = req.socket.params.DOCUMENT_ROOT
  const pathinfo = req.socket.params.PATH_INFO

  // (Assume PATH_INFO is URL-decoded, courtesy of Apache or FCGI specs)
  // Disallow if..
  if (
    // .. docroot or requested script path aren't set
    !docroot || !pathinfo ||
    // .. requested script path doesn't end in '.js'
    !pathinfo.endsWith('.js') ||
    // .. requested script path doesn't start with '/' (safe-guard, because it's combined with docroot)
    !pathinfo.startsWith('/') ||
    // .. requested script path includes '../' (to avoid directory climbing. safe-guard, because apache/fcgi specs ought have stripped it)
    pathinfo.includes('../') ||
    // .. requested script path includes '/.' (to avoid access to dot-files)
    pathinfo.includes('/.') ||
    // .. requested script path includes '//' (to avoid some obscure and possibly imaginary root-access)
    pathinfo.includes('//')
  ) return ''

  // Produce filepath to the requested script file.
  return docroot + pathinfo
}
