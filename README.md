# Use NodeJS via FastCGI

The purpose of this FastCGI program (`nodejs.fcgi`) is to execute the .js file given to it in [PATH_INFO][PATH_INFO] (relative to [DOCUMENT_ROOT][DOCUMENT_ROOT]).

> An HTTP request of the form  
> `http://example.com/nodejs.fcgi/path/to/script.js`  
> would end up executing a NodeJS script in the filepath  
> `DOCUMENT_ROOT/path/to/script.js`

The server that is serving HTTP-request must be configured to execute files with `.fcgi` extension as FastCGI programs.

The provided `.htaccess` files are for [Apache][Apache], but this project works without them. You are adviced to duplicate their functionality if you aren't using Apache.

[PATH_INFO]: https://en.wikipedia.org/wiki/Common_Gateway_Interface
[DOCUMENT_ROOT]: https://httpd.apache.org/docs/current/mod/mod_cgi.html
[Apache]: https://en.wikipedia.org/wiki/Apache_HTTP_Server


## Usage:

(*The folders given are an example.*)

1. Get yourself the NodeJS binary, for example from https://nodejs.org/en/download/

 **Important!** The extracted NodeJS files must be accessible by the program executing `nodejs.fcgi` (which is provided in this repository).

 ```
 $ cd '/var/www/userhome/<user>/sites/.data/programs/'
 $ curl -O 'https://nodejs.org/dist/v4.6.0/node-v4.6.0-linux-x64.tar.xz'
 $ untar xvJf 'node-v4.6.0-linux-x64.tar.xz'
 ```

2. Copy this repository's content to to your web server's root.
 ```
 $ cd '/var/www/userhome/<user>/sites/<example.com>/www/'
 $ git clone https://github.com/gima/nodejs-fcgi
 ```

3. Edit the first line of `nodejs.fcgi` from:
 ```
 #!/path/to/node
 to:
 #!/var/www/userhome/<user>/sites/.data/programs/node-v4.6.0-linux-x64/bin/node
 ```

4. Copy the provided `.htaccess-wwwdir` file to directories where you want `.js` files to be executed by NodeJS:
```
$ cp .htaccess-wwwdir '/var/www/.../<example.com>/www/test/.htaccess'
```

Done.

Now, for example if the following file existed:  
`/var/www/userhome/<user>/sites/<example.com>/www/test/test.js`:
```js
module.exports = function(req,res) {
  res.writeHead(200, { 'Content-Type': 'text/plain' })
  res.write('Hello World!')
  res.end()
}
```

Then a HTTP request to it would work like so:
```sh
$ curl -i 'http://example.com/test/test.js'
HTTP/1.1 200 OK
...
Server: Apache/2.4
Transfer-Encoding: chunked
Content-Type: text/plain

Hello World!
```


## Content of repository:

```
-rwxr-xr-x  nodejs.fcgi*
```
The actual program executed by the FastCGI executor (Apache, for example).

```
drwxr-xr-x  node_modules/
```
Libraries used by `nodejs.fcgi`.

```
-rw--w--w-  .nodejs-fcgi.log
```
Empty log file, written to by `nodejs.fcgi` in case of uncaught exceptions.

```
-rw-r--r--  .htaccess
```
Denies unnecessary HTTP access to the contents of this directory (for Apache).


## License

[MIT](LICENSE)
